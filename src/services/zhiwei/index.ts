// @ts-ignore
/* eslint-disable */
// API 更新时间：
// API 唯一标识：
import * as role from './role';
import * as user from './user';
import * as userConfig from './userConfig';
import * as tenant from './tenant';
import * as api from './api';
import * as project from './project';
import * as pipeline from './pipeline';
import * as pipelineTask from './pipelineTask';
import * as pipelineLog from './pipelineLog';
import * as pipelineGroup from './pipelineGroup';
import * as parameter from './parameter';
import * as menu from './menu';
import * as me from './me';
import * as generateCode from './generateCode';
import * as devEntity from './devEntity';
import * as department from './department';
import * as data from './data';
import * as auth from './auth';
import * as app from './app';
import * as appApi from './appApi';
import * as portal from './portal';
import * as pipelineHistory from './pipelineHistory';
import * as devEntityProperty from './devEntityProperty';
import * as bizData from './bizData';
export default {
  role,
  user,
  userConfig,
  tenant,
  api,
  project,
  pipeline,
  pipelineTask,
  pipelineLog,
  pipelineGroup,
  parameter,
  menu,
  me,
  generateCode,
  devEntity,
  department,
  data,
  auth,
  app,
  appApi,
  portal,
  pipelineHistory,
  devEntityProperty,
  bizData,
};
